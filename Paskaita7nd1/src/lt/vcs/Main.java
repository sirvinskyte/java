package lt.vcs;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Gabriele
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        String line = inLine("Iveskite zodzius atskirtus kableliu");
        String[] mas = line.replaceAll(" ", "").split(",");
        List <String> listas = Arrays.asList(mas);
        Collections.sort(listas);
        out("Zodziai:");
        for(String i : listas){
            out(i);
        }
    }
    
}
