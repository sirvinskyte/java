/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.Arrays;
import static lt.vcs.VcsUtils.*;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //b)
        
       String zodziai = inStr("ivesk kiek nori zodziu atskirtu per kableli");
       String[] strMas = zodziai.split(",");
       for(int i=0; i<=strMas.length-1; i++){
           out((i+1)+" "+strMas[i]);
       }
       
        //c) 
        
        String zodis = inLine("iveskite zodi/sakini");
        char[] strZod =zodis.replaceAll(" ", "").replaceAll(",", "").toCharArray();
        int x=0;
        int y=0;
        for(int i=0; i<=strZod.length-1; i++){
            x++;
            
            if(strZod[i]=='a'){
                y++;
            }
        }
        out("is viso raidziu yra:"+x+", o raidziu a: "+y);
       
    }
    
}
