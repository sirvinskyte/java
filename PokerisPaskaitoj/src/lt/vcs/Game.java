package lt.vcs;

import static lt.vcs.GameUtils.*;
import static lt.vcs.VcsUtils.*;

/**
 * Zaidimo klase, reprezentuojanti viena zaidimo partija
 */
public class Game {

    /** pirmas zaidejas */
    private final Player p1;
    /** antras zaidejas */
    private final Player p2;

    private Player activePlayer;

    /**
     * zaidimo konstruktorius
     * @param p1 pirmas zaidejas
     * @param p2 antras zaidejas
     */
    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        activePlayer = p1;
    }

    /**
     * startuoja zaidima/partija
     * @return 
     */
    public Player start() {
        
        int p1Bet = inInt(activePlayer.getName()+" iveskite statoma suma");
        int p2Bet = inInt(nextActive().getName()+" iveskite statoma suma");
        activePlayer.setCash(activePlayer.getCash()-p1Bet);
        nextActive().setCash(activePlayer.getCash()-p1Bet);
      
        if(p1Bet<p2Bet){
            out(activePlayer.getName()+" priesininkas pakele suma iki "+p2Bet);
            int choice = inInt("Ka darysite? 0-islyginsite; 1-foldinti");
            if(choice==1){
                nextActive().setCash(nextActive().getCash()+p1Bet+p2Bet);
                return nextActive();
            }
        }
        int pot = p1Bet + p2Bet;
        activePlayer.setHand(new Hand (rollHand()));
        out(activePlayer.getName()+" jusu ranka: "+intArrayToString(activePlayer.getHand().getHandArray()));
        nextActive().setHand(new Hand(rollHand()));
        out(nextActive().getName() + " jusu ranka: " + intArrayToString(nextActive().getHand().getHandArray()));
               
        Player laimetojas = GameUtils.kasLaimejo(p1, p2);
        out("Laimetojas"+laimetojas.getName()+" , sveikiname!");
        int totalPot = getTotalPot(pot, laimetojas.getHand().getCombination());
        out("Laimejimo suma: "+totalPot);
        laimetojas.setCash(laimetojas.getCash()+totalPot);
        return laimetojas;
        //TODO: cia turetu prasideti visa pagrindine zaidimo mechanika gal cia ir uzsibaigti, nuo jusu priklauso :)
    }
    
    private int getTotalPot(int pot, Combination combo){
        return pot+combo.getBonus();
    }

    private Player nextActive(){
        return Main.getNextActivePlayer(this);
    }
    public Player getP1() {
        return p1;
    }

    public Player getP2() {
        return p2;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }
}
