
package lt.vcs;

import static java.awt.SystemColor.info;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Gabriele
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "vcs";
        String dbUser = "root";
        String dbPass = "";
        Connection conn = DriverManager.getConnection(url+dbName, dbUser, dbPass);
        if(conn != null){
            out("valio, prisijungem prie " + url+dbName);
        }
        String name = inStr("Iveskite varda");
        String surname = inStr("Iveskite pavarde");
        int age = inInt("Iveskite amziu");
//        int cash = inInt("Iveskite jusu turima pinigu suma");
        
        Person p = new Person(getLastPersonId(conn)+1, name, surname, age);
        if(p.save(conn)){
            out("Person("+ p.getId() + ") objektas issaugotas");
        } else{
            out("Person objekto issaugot nepavyko");
        }
        conn.commit();
        conn.close();
        
    }
    
    private static int getLastPersonId (Connection conn) throws Exception{
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select id from person order by id desc");
        rs.next();
        return rs.getInt("id");
    }
    
    
}
