package lt.vcs;

import static lt.vcs.GameUtils.*;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Player.*;
import static lt.vcs.Game.*;

/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        out("Kauliuku pokeris");
        String p1name = inStr("Zaidejas 1, iveskite savo varda");
        String p2name = inStr("Zaidejas 2, iveskite savo varda");
        
        Player p1 = new Player(p1name);
        Player p2 = new Player(p2name);
        String zaisti = inStr("Ar norite ziasti? taip/ne");
        
        while ("taip".equals(zaisti)) {
            if(p1.getCash()>0&&p2.getCash()>0){
            Game newGame = new Game(p1, p2);
            newGame.start();
            
                zaisti = inStr("Ar norite ziasti? taip/ne");
            }
            else{
                out("Nebeturite pinigu");
                return;
            }
            
            //TODO toliau jusu kodas :) arba galite persitvarkyti struktura kaip jum patogiau
            
        }
        
    }
    
    private static int rollDice() {
        return random(1, 6);
    }
    
    /**
     * perridena norimus kauliukus ir perskaiciuoja kombinacija ir kitus skaicius
     * @param dices kauliuku skaiciai, atskirti kableliu, kuriuos norim perridenti
     */
    public static void reRollDice(int[] hand, String dices) {
        dices = dices.replaceAll(" ", "");
        for (String dice : dices.split(",")) {
            Integer nr = new Integer(dice);
            hand[nr - 1] = rollDice();
        }
    }

    private static void Player(String p1name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
