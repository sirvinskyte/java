package lt.vcs;

import java.util.Arrays;
import static lt.vcs.GameUtils.*;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Combination.*;
import static lt.vcs.HandUtils.*;
import static lt.vcs.Hand.*;

public class Game {

    /** pirmas zaidejas */
    private final Player p1;
    /** antras zaidejas */
    private final Player p2;

    private Player activePlayer;

    /**
     * zaidimo konstruktorius
     * @param p1 pirmas zaidejas
     * @param p2 antras zaidejas
     */
    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        activePlayer = p1;
    }

    /**
     * startuoja zaidima/partija
     */
    public void start() {
        
        int suma1 = inInt(""+p1.getStats()+", Iveskite suma, kuria statote");
        if(suma1>(p1.getCash())){
            out("neturite tiek pinigu");
            return;    
        }
        int suma2 = inInt(""+p2.getStats()+", Iveskite suma, kuria statote; Islyginsite arba pakelkite");
        if(suma2>(p2.getCash())){
            out("neturite tiek pinigu");
            return;
      
        }
        else if(suma2<suma1){
            out(""+p2.getName()+", Turite arba islyginti arba pakelti suma");
            suma2 = inInt();
            
        }
        while(suma2!=suma1){
                suma1 = inInt(""+p1.getName()+", Iveskite suma, kuria statote; Islyginsite arba pakelkite");
                if(suma1>suma2){
                 suma2 = inInt(""+p2.getName()+", Iveskite suma, kuria statote; Islyginsite arba pakelkite");

                }
            }
               
        out(suma1+" "+suma2);

        out(p1.getName()+" jusu ranka: "+intArrayToString(p1.getHand().getHandArray()));
        out(p2.getName()+" jusu ranka: "+intArrayToString(p2.getHand().getHandArray()));

        
           
        
        Player laimetojas = GameUtils.kasLaimejo(p1, p2);
        out("Laimejo "+laimetojas.getName());
        
          Hand handl = laimetojas.getHand();
          int x = handl.getCombination().getBonus();
          out("Bonusas:"+x);

          int pot = suma1+suma2;
       
            int sumag = laimetojas.getCash()+x;
            laimetojas.setCash(sumag+pot);
        
        int likutis1 = p1.getCash()-suma1;
        int likutis2 = p2.getCash()-suma2;
        
        

        out(""+p1.getName()+" likutis: "+likutis1);
        out(""+p2.getName()+" likutis: "+likutis2);
        
        p1.setCash(likutis1);
        p2.setCash(likutis2);
       

        //TODO: cia turetu prasideti visa pagrindine zaidimo mechanika gal cia ir uzsibaigti, nuo jusu priklauso :)
    }

}
