/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Gabriele
 */
public interface Creature {
    /**
     * 
     * metodas, kuris nusako, kur butybe gyvena, ar danguje, ar zemeje, ar vandenyje
     */
    //public abstract galima nerasyti, automatiskai sugeneruojama
    public abstract String getWorld();
    
}
