package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Deimantas.*;
/**
 *
 * @author Gabriele
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String words = inLine("Iveskite zodzius atskirtus kableliu");
        String[] wordMas = words.replaceAll(" ", "").split(",");
        int [] mas = new int[3];
        int [][] intMas = {
            {1,2,3},
            {4,5,6},
            {7,8,9}
        };
        out(""+intMas[1][1]);
        
        List<String> pvz = new ArrayList();
        pvz.add("kazkas");
        List <String> listas = toSortedList(wordMas);
        List<Integer> inList = toSortedList(3,2,1);
        out("surusiuoti zodziai liste");
        for(String word : listas){
            out(word);
        }
        Collections.sort(listas);//isrusiuoja pagal abecele
        Set <String> setas = new TreeSet(listas);//istrina pasikartojancius zodzius
        out("Surusiuoti zodziai");
        for(String word : setas){
            out(word);
        }
        
        Map<String, List<String>> mapas = new HashMap();
        List<String> neSu = Arrays.asList("bananas", "ananasas", "agurkas");
        mapas.put("nesurusiuotas", neSu);
        mapas.put("surusiuotas", listas);
        for(String word : mapas.get("nesurusiuotas")){
            out(word);
        }
        
        Deimantas<List<Integer>> bla = null;
        
    }
    private static <E> List<E> toSortedList(E... mas){
        List listas = Arrays.asList(mas);
        Collections.sort(listas);
        return listas;
    }
    
}
